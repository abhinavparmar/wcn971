﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Znode.Engine.Api.Client;
using Znode.Engine.ERPConnector.S2KConnector.S2KHelper;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.ERPConnector.S2KConnector
{
    public class ZnodeS2KConnector : BaseERP
    {
        private readonly ZnodeRepository<ZnodeImportHead> _importHeadRepository;
        private readonly ZnodeRepository<ZnodeERPConfigurator> _eRPConfiguratorRepository;
        private readonly OrderClient _orderClient;
        private readonly ZnodeRepository<ZnodeOmsOrder> _omsOrderRepository;
        private readonly ZnodeRepository<ZnodeImportTemplate> _importTemplateRepository;
        private readonly ZnodeRepository<ZnodeImportProcessLog> _importProcessLogRepository;
        private readonly WCNShoppingFeedServiceHelper _wCNShoppingFeedServiceHelper;

        public ZnodeS2KConnector()
        {
            _importHeadRepository = new ZnodeRepository<ZnodeImportHead>();
            _eRPConfiguratorRepository = new ZnodeRepository<ZnodeERPConfigurator>();
            _orderClient = GetClient<OrderClient>();
            _omsOrderRepository = new ZnodeRepository<ZnodeOmsOrder>();
            _importTemplateRepository = new ZnodeRepository<ZnodeImportTemplate>();
            _importProcessLogRepository = new ZnodeRepository<ZnodeImportProcessLog>();
            _wCNShoppingFeedServiceHelper = new WCNShoppingFeedServiceHelper();
        }

        [RealTime("RealTime")]
        public string UpdateOrder()
        {
            return "";
        }

        public override bool ARHistory() => true;

        //AR Payment Details from ERP to ZNODE
        public override bool ARPaymentDetails() => true;

        //AR Balance from ERP to ZNODE
        public override bool ARBalance() => true;

        //Refresh Product Content from ERP to Znode
        public override bool ProductRefresh() => true;

        //Refresh Category from ERP to Znode
        public override bool CategoryRefresh() => true;

        //Refresh Product Category Link from ERP to Znode                            
        public override bool ProductCategoryLinkRefresh() => true;

        //Refresh  Contact List from ERP to Znode          
        public override bool ContactListRefresh() => true;

        //Get Contact List on real-time from ERP to Znode   
        public override bool GetContactList() => true;

        //Refresh Contact Details from ERP to Znode           
        public override bool ContactDetailsRefresh() => true;

        //Get Contact Details on real-time from   ERP to Znode  
        public override bool GetContactDetails() => true;

        //Get Login from ZNODE to ERP
        public override bool Login() => true;

        //Create Contact from ZNODE to ERP
        public override bool CreateContact() => true;

        //Create Update from ZNODE to ERP
        public override bool UpdateContact() => true;

        public override bool PaymentAuthorization() => true;
        public override bool SaveCreditCard() => true;

        //Refresh customer details from ERP to Znode
        public override bool CustomerDetailsRefresh() => true;

        //Get Customer Details on real-time from   ERP to Znode  
        public override bool GetCustomerDetails() => true;

        //Refresh ShipToList from ERP to Znode
        public override bool ShipToListRefresh() => true;

        //Get ShipToList on real-time from   ERP to Znode  
        public override bool GetShipToList() => true;

        //Locate My Account Or Match Customer from Znode to ERP
        public override bool LocateMyAccountOrMatchCustomer() => true;

        //Create Customer from Znode to ERP
        public override bool CreateCustomer() => true;

        //Update Customer from Znode to ERP
        public override bool UpdateCustomer() => true;

        //Create SHIPTO from Znode to ERP
        //public override bool CreateSHIPTO() => true;

        //Update SHIPTO from Znode to ERP
        //public override bool UpdateSHIPTO() => true;
        //Get inventory on real-time from ERP to Znode              
        public override bool GetInventoryRealtime() => true;

        //Get Invoice History from ERP to Znode       
        public override bool InvoiceHistory() => true;

        //Get Invoice Details Status from ERP to Znode       
        public override bool InvoiceDetailsStatus() => true;

        public override bool RequestACatalog() => true;

        //Submit A Prospect from ERP to Znode       
        public override bool SubmitAProspect() => true;

        //OrderSimulate from Znode  to ERP      
        public override bool OrderSimulate() => true;

        //Create Order from Znode  to ERP   
        public override bool OrderCreate() => true;

        //Get Order History from ERP to Znode       
        public override bool OrderHistory() => true;

        //Get Order Details Status from ERP to Znode       
        public override bool OrderDetailsStatus() => true;

        //Pay Online from Znode to ERP
        public override bool PayOnline() => true;
        //Refresh customer / contract price list from ERP to Znode on a scheduled basis.
        public override bool PricingCustomerPriceListRefresh() => true;

        //Get list or customer specifi price on real-time from ERP to Znode      
        public override bool GetPricing() => true;

        //Create Quote from Znode  to ERP   
        public override bool QuoteCreate() => true;

        //Get Quote History from ERP to Znode 
        public override bool QuoteHistory() => true;

        public override bool ShippingOptions() => true;
        public override bool ShippingNotification() => true;

        //Tax calculation from ERP to Znode 
        public override bool TaxCalculation() => true;

        public virtual bool ShoppingFeed()
        {
            bool status = false;
            try
            {
                ZnodeLogging.LogMessage("UpdatePriceAndInventory Start", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                _wCNShoppingFeedServiceHelper.GenerateShoppingFeed();
                status = true;
                ZnodeLogging.LogMessage("UpdatePriceAndInventory End", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
            }
            catch (Exception ex)
            {
                status = false;
                ZnodeLogging.LogMessage("UpdatePriceAndInventory Error!!-" + ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
            }
            return status;
        }
    }
}
