﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.ERPConnector.S2KConnector.S2KModel;
using Znode.Engine.Exceptions;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Engine.ERPConnector.S2KConnector.S2KHelper
{
    public class WCNShoppingFeedServiceHelper
    {
        private readonly string _ShoppingFeedFilePath;
        private readonly string _ShoppingFeedFileName;
        private readonly string _ShoppingFeedHost;
        private readonly string _ShoppingFeedUserName;
        private readonly string _ShoppingFeedPassword;
        private readonly string _ShoppingFeedApiDomainName;
        private readonly string _ShoppingFeedWebStoreDomainName;
        private readonly string _ShoppingFeedNotificationEmail;
        #region Constructor
        public WCNShoppingFeedServiceHelper()
        {
            _ShoppingFeedFilePath = Convert.ToString(ConfigurationManager.AppSettings["ShoppingFeedPath"]);
            _ShoppingFeedFileName = Convert.ToString(ConfigurationManager.AppSettings["ShoppingFeedFileName"]);
            _ShoppingFeedHost = Convert.ToString(ConfigurationManager.AppSettings["ShoppingFeedHost"]);
            _ShoppingFeedUserName = Convert.ToString(ConfigurationManager.AppSettings["ShoppingFeedUserName"]);
            _ShoppingFeedPassword = Convert.ToString(ConfigurationManager.AppSettings["ShoppingFeedPassword"]);
            _ShoppingFeedApiDomainName = Convert.ToString(ConfigurationManager.AppSettings["ShoppingFeedApiDomainName"]);
            _ShoppingFeedWebStoreDomainName = Convert.ToString(ConfigurationManager.AppSettings["ShoppingFeedWebStoreDomainName"]);
            _ShoppingFeedNotificationEmail = Convert.ToString(ConfigurationManager.AppSettings["HawkFeedNotificationEmail"]);
        }
        #endregion
        public bool GenerateShoppingFeed()
        {
            ZnodeLogging.LogMessage(" Shopping Feed creation process Started : " + DateTime.Now.ToShortDateString(), ZnodeLogging.Components.ERP.ToString());
            var result = CreateShoppingFeed();
            ZnodeLogging.LogMessage(" Shopping Feed creation process End : " + DateTime.Now.ToShortDateString(), ZnodeLogging.Components.ERP.ToString());
            return result;
        }

        public bool CreateShoppingFeed()
        {
            bool result = true;

            try
            {
                List<WCNProductShoppingFeedModel> _ProductsForShoppingFeed = GetProductsForShoppingFeed();
                List<WCNCategoryShoppingFeedModel> _CategoryForShoppingFeed = GetCategoryForShoppingFeed();
                List<WCNSeoShoppingFeedModel> _SeoForShoppingFeed = GetSeoForShoppingFeed();
                List<WCNPriceShoppingFeedModel> _PriceForShoppingFeed = GetPriceForShoppingFeed();
                List<WCNProductImageShoppingFeedModel> _ProductImageForShoppingFeed = GetProductImageForShoppingFeed();
                List<WCNProductWeightShoppingFeedModel> _ProductWeightForShoppingFeed = GetProductWeightForShoppingFeed();
                GenerateShoppingFeedFile(_ProductsForShoppingFeed, _CategoryForShoppingFeed, _SeoForShoppingFeed, _PriceForShoppingFeed, _ProductImageForShoppingFeed, _ProductWeightForShoppingFeed);
            }
            catch (Exception ex)
            {
                result = false;
                ZnodeLogging.LogMessage(ex.Message.ToString(), ZnodeLogging.Components.ERP.ToString());
            }

            if (result)
            {
                ZnodeLogging.LogMessage("Shopping Feed Created Successfully: " + DateTime.Now.ToString(), ZnodeLogging.Components.ERP.ToString());
                //UploadShoppingFeed();
            }

            return result;
        }

        private void UploadShoppingFeed()
        {
            try
            {
                byte[] fileBytes = null;
                using (StreamReader fileStream = new StreamReader(_ShoppingFeedFilePath + _ShoppingFeedFileName))
                {
                    fileBytes = Encoding.UTF8.GetBytes(fileStream.ReadToEnd());
                    fileStream.Close();
                }

                //Create FTP Request.
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(_ShoppingFeedHost + _ShoppingFeedFileName);
                request.Method = WebRequestMethods.Ftp.UploadFile;

                //Enter FTP Server credentials.
                request.Credentials = new NetworkCredential(_ShoppingFeedUserName, _ShoppingFeedPassword);
                request.ContentLength = fileBytes.Length;
                request.UsePassive = true;
                request.UseBinary = true;
                request.ServicePoint.ConnectionLimit = fileBytes.Length;

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(fileBytes, 0, fileBytes.Length);
                    requestStream.Close();
                }

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                //Trigger email.
                ZnodeEmail.SendEmail(_ShoppingFeedNotificationEmail, ZnodeConfigManager.SiteConfig.AdminEmail, " PPC/ Shopping Feeds have been successfully uploaded to FTP.", string.Empty);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Failed to upload shopping feed: " + DateTime.Now.ToString() + " " + ex.InnerException?.ToString(), ZnodeLogging.Components.ERP.ToString());
            }
        }

        private void GenerateShoppingFeedFile(List<WCNProductShoppingFeedModel> _ProductsForShoppingFeed, 
            List<WCNCategoryShoppingFeedModel> _CategoryForShoppingFeed, List<WCNSeoShoppingFeedModel> _SeoForShoppingFeed, 
            List<WCNPriceShoppingFeedModel> _PriceForShoppingFeed, List<WCNProductImageShoppingFeedModel> _ProductImageForShoppingFeed, 
            List<WCNProductWeightShoppingFeedModel> _ProductWeightForShoppingFeed)
        {
            string formatString = "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\t{18}\t{19}\t{20}\t{21}\t{22}\t{23}" +
                 "{24}\t{25}\t{26}\t{27}\t{28}\t{29}\t{30}\t{31}\t{32}\t{33}\t{34}\t{35}\t{36}\t{37}\t{38}\t{39}\t{40}\t{41}\n";

            using (StreamWriter productFile = new StreamWriter(_ShoppingFeedFilePath + _ShoppingFeedFileName))
            {
                StringBuilder strBuilder = new StringBuilder();

                //if (!isNewFile)
                strBuilder.Append(string.Format(formatString,
               "id",  //0
               "title",   //1
               "description",    //2
               "google_product_category",  //3
               "product_type",  //4
               "link", //5
               "image_link", //6
               "additional_image_link", //7
               "condition",  //8
               "availability",  //9
               "price",   //10
               "brand",  //11 
               "gtin",  //12
               "mpn",   //13
               "identifier_exists", //14
               "gender", //15
               "age_group", //16
               "color", //17 
               "size", //18
               "item_group_id",//19
               "material", //20
               "pattern", //21
               "tax", //22
               "shipping", //23
               "shipping_weight", //24
                "multipack",//25
                "adult", //26
                "adwords_grouping",//27
                "adwords_labels",//28
                "adwords_redirect",//29
                "custom_label_0",//30
                "custom_label_1",//31
                "custom_label_2",//32
                "custom_label_3",//33
                "custom_label_4",//34
                "c:extended_description:string",//35
                "c:keywords:string",//36
                "json_tier_pricing",//37
                "qty",//38
                "status",//39
                "stock_status",//40
                "parent_category"//41
               ));

                foreach (WCNProductShoppingFeedModel item in _ProductsForShoppingFeed)
                {
                    var categoryName = _CategoryForShoppingFeed.Where(x => x.Sku == item.Sku).FirstOrDefault();
                    var seoData = _SeoForShoppingFeed.Where(x => x.SeoCode == item.Sku).FirstOrDefault();
                    var priceData = _PriceForShoppingFeed.Where(x => x.Sku == item.Sku).FirstOrDefault();
                    var productImageData = _ProductImageForShoppingFeed.Where(x => x.Sku == item.Sku).FirstOrDefault();
                    var productWeightData = _ProductWeightForShoppingFeed.Where(x => x.Sku == item.Sku).FirstOrDefault();
                    string priceTier = CreateTierPrice(_PriceForShoppingFeed.Where(x => x.Sku == item.Sku).ToList());

                    strBuilder.Append(string.Format(formatString,
                          string.IsNullOrEmpty(item?.Sku)? "": item?.Sku, //0
                          string.IsNullOrEmpty(item?.ProductName) ? "" : item?.ProductName,  //1
                          "<p> " + (string.IsNullOrEmpty(seoData?.SeoDescription) ? "" : seoData?.SeoDescription) + " <p>",//2
                         string.IsNullOrEmpty(categoryName?.CategoryName) ? "" : categoryName?.CategoryName,//3
                         string.IsNullOrEmpty(categoryName?.CategoryName) ? "" : categoryName?.CategoryName,  //4
                         _ShoppingFeedWebStoreDomainName + "/" + (string.IsNullOrEmpty(seoData?.SeoUrl) ? "" : seoData?.SeoUrl), //5
                         _ShoppingFeedApiDomainName + (string.IsNullOrEmpty(productImageData?.MediaPath) ? "" : productImageData?.MediaPath) + (string.IsNullOrEmpty(productImageData?.ProductImage) ? "" : productImageData?.ProductImage), //6
                          string.Empty, //7 -additional_image_link
                           string.Empty,  //8 -condition
                           string.Empty,  //9 -availability
                           string.IsNullOrEmpty(priceData?.RetailPrice) ? "" : priceData?.RetailPrice, //10 
                           "WCN",//ValueFromSelectValues(item.Attributes, ZnodeConstant.Brand),  //11 
                           string.Empty, //item.UPC,  //12-gtin
                           string.Empty,   //13 -mpn
                           string.Empty,  //14 -identifier_exists
                           string.Empty,  //15-gender
                           string.Empty,   //16 -age_group
                           string.Empty,  //17 
                           string.IsNullOrEmpty(productWeightData?.ProductWeight) ? "" : productWeightData?.ProductWeight,  //18
                           string.Empty,  //19 - item_group_id
                           string.Empty,  //20 -material
                           string.Empty,  //21 -pattern
                           string.Empty, //22 -tax
                           string.Empty, //23 -shipping
                           string.IsNullOrEmpty(productWeightData?.ProductWeight) ? "" : productWeightData?.ProductWeight, //24 -shipping_weight
                           string.Empty,//25 -multipack
                           string.Empty, //26 -adult
                           string.Empty,//27 -adwords_grouping
                           string.Empty,//28 -adwords_labels
                           _ShoppingFeedApiDomainName + "/" + (string.IsNullOrEmpty(seoData?.SeoUrl) ? "" : seoData?.SeoUrl) + (string.IsNullOrEmpty(seoData?.GoogleAdSenseUrl) ? "" : seoData?.GoogleAdSenseUrl),//29-adwords_redirect -- have to create the url.
                           string.Empty,//30 -custom_label_0
                           string.Empty,//31 -custom_label_1
                           string.Empty,//32 -custom_label_2
                           string.Empty,//33-custom_label_3
                           string.Empty,//34-custom_label_4
                          string.Empty,//35 -c:extended_description:string
                          string.Empty,//36 -c:keywords:string
                           string.IsNullOrEmpty(priceTier) ? "" : priceTier,//37 -"json_tier_pricing"
                           string.Empty,  //38 - qty
                           "1",  //39 - status
                           "1", //40 - stock_status
                           string.IsNullOrEmpty(categoryName?.CategoryName) ? "" : categoryName?.CategoryName //41 parent_category
                      ));
                }
                productFile.WriteLine(Convert.ToString(strBuilder));
                productFile.Close();
            }
        }

        private List<WCNProductShoppingFeedModel> GetProductsForShoppingFeed()
        {
            IZnodeViewRepository<WCNProductShoppingFeedModel> objStoredProc = new ZnodeViewRepository<WCNProductShoppingFeedModel>();
            
            List<WCNProductShoppingFeedModel> _WCNProductShoppingFeedModel = objStoredProc.ExecuteStoredProcedureList("Nivi_GetWCNShoppingFeedProductData").ToList();
            return _WCNProductShoppingFeedModel;
        }

        private List<WCNCategoryShoppingFeedModel> GetCategoryForShoppingFeed()
        {
            IZnodeViewRepository<WCNCategoryShoppingFeedModel> objStoredProc = new ZnodeViewRepository<WCNCategoryShoppingFeedModel>();

            List<WCNCategoryShoppingFeedModel> _WCNCategoryShoppingFeedModel = objStoredProc.ExecuteStoredProcedureList("Nivi_GetWCNShoppingFeedCategoryData").ToList();
            return _WCNCategoryShoppingFeedModel;
        }

        private List<WCNSeoShoppingFeedModel> GetSeoForShoppingFeed()
        {
            IZnodeViewRepository<WCNSeoShoppingFeedModel> objStoredProc = new ZnodeViewRepository<WCNSeoShoppingFeedModel>();

            List<WCNSeoShoppingFeedModel> _WCNSeoShoppingFeedModel = objStoredProc.ExecuteStoredProcedureList("Nivi_GetWCNShoppingFeedSeoData").ToList();
            return _WCNSeoShoppingFeedModel;
        }

        private List<WCNPriceShoppingFeedModel> GetPriceForShoppingFeed()
        {
            IZnodeViewRepository<WCNPriceShoppingFeedModel> objStoredProc = new ZnodeViewRepository<WCNPriceShoppingFeedModel>();

            List<WCNPriceShoppingFeedModel> _WCNPriceShoppingFeedModel = objStoredProc.ExecuteStoredProcedureList("Nivi_GetWCNShoppingFeedPriceData").ToList();
            return _WCNPriceShoppingFeedModel;
        }

        private List<WCNProductImageShoppingFeedModel> GetProductImageForShoppingFeed()
        {
            IZnodeViewRepository<WCNProductImageShoppingFeedModel> objStoredProc = new ZnodeViewRepository<WCNProductImageShoppingFeedModel>();

            List<WCNProductImageShoppingFeedModel> _WCNProductImageShoppingFeedModel = objStoredProc.ExecuteStoredProcedureList("Nivi_GetWCNShoppingFeedImageData").ToList();
            return _WCNProductImageShoppingFeedModel;
        }

        private List<WCNProductWeightShoppingFeedModel> GetProductWeightForShoppingFeed()
        {
            IZnodeViewRepository<WCNProductWeightShoppingFeedModel> objStoredProc = new ZnodeViewRepository<WCNProductWeightShoppingFeedModel>();

            List<WCNProductWeightShoppingFeedModel> _WCNProductWeightImageShoppingFeedModel = objStoredProc.ExecuteStoredProcedureList("Nivi_GetWCNShoppingFeedWeightData").ToList();
            return _WCNProductWeightImageShoppingFeedModel;
        }

        private string CreateTierPrice(List<WCNPriceShoppingFeedModel> skuPriceList)
        {
            if (skuPriceList == null ||
                skuPriceList.Count == 0)
                return "";

            string priceTier = "[";
            int count = 1;
            foreach (var item in skuPriceList)
            {
                if (count == 1)
                {
                    priceTier = priceTier + string.Format("[\"{0}\",\"{1}\"],", "1", item?.RetailPrice);
                }
                else
                {
                    priceTier = priceTier + string.Format("[\"{0}\",\"{1}\"],", "12", item?.RetailPrice);
                }
                count++;
            }
            priceTier = priceTier.Substring(0, priceTier.Length - 1) + "]";

            return priceTier;
        }
    }
}