﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.ERPConnector.S2KConnector.S2KModel
{
    public class WCNSeoShoppingFeedModel
    {
        public string SeoTitle { get; set; }
        public string SeoDescription { get; set; }
        public string SeoUrl { get; set; }
        public string SeoCode { get; set; }
        public string GoogleAdSenseUrl { get; set; }
    }
}
