﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.ERPConnector.S2KConnector.S2KModel
{
    public class WCNCategoryShoppingFeedModel
    {
        public string Sku { get; set; }
        public string CategoryName { get; set; }
    }
}
