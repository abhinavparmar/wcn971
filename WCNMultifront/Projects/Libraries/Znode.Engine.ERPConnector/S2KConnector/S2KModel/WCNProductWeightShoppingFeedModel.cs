﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.ERPConnector.S2KConnector.S2KModel
{
    public class WCNProductWeightShoppingFeedModel
    {
        public string Sku { get; set; }
        public string ProductWeight { get; set; }
    }
}
