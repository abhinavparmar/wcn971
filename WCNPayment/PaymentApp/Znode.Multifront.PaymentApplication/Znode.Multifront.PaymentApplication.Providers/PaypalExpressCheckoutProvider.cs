﻿using System;
using Znode.Multifront.PaymentApplication.Models;
using Znode.Libraries.Paypal;
namespace Znode.Multifront.PaymentApplication.Providers
{
    public class PaypalExpressCheckoutProvider : BaseProvider, IPaymentProviders
    {
        public GatewayResponseModel ValidateCreditcard(PaymentModel paymentModel)
            =>  new PaypalGateway(paymentModel).DoExpressCheckoutPayment(paymentModel.GatewayToken, paymentModel.CardDataToken);

        public GatewayResponseModel Refund(PaymentModel paymentModel)
            => new PaypalGateway(paymentModel).Refund(paymentModel);

        public GatewayResponseModel Void(PaymentModel paymentModel)
            => new PaypalGateway(paymentModel).Void(paymentModel);

        public GatewayResponseModel Subscription(PaymentModel paymentModel)
        {
            throw new NotImplementedException();
        }

        public TransactionDetailsModel GetTransactionDetails(PaymentModel paymentModel)
        {
            return new TransactionDetailsModel();
        }
    }
}